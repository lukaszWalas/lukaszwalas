package com.sda.organizer.repository;

import com.sda.organizer.domain.Recipe;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by RENT on 2016-10-01.
 */
public interface RecipeRepository extends MongoRepository<Recipe, String> {

    Recipe findByTitle(String title);
    Recipe findByCategory (Enum category);

}

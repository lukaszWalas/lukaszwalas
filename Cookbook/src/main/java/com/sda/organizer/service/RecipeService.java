package com.sda.organizer.service;

import com.sda.organizer.domain.Recipe;
import com.sda.organizer.enums.Category;
import com.sda.organizer.repository.RecipeRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

/**
 * Created by RENT on 2016-09-27.
 */
@Service
public class RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;
    @FXML
    VBox breakfastAccordion;
    @FXML
    VBox dinnerAccordion;
    @FXML
    VBox supperAccordion;


    public List<Recipe> getAll() {
        return recipeRepository.findAll();
    }

    public void addNew(Recipe recipe) {
        recipeRepository.save(recipe);
    }

    public void bindByCategory() {
        List<Recipe> all = recipeRepository.findAll();
        for(Recipe r : all) {
            if(r.getCategory().equals(Category.SNIADANIE)){
                Button button = new Button();
                button.setText(r.getTitle());
                breakfastAccordion.getChildren().add(button);

            }else if (r.getCategory().equals(Category.OBIAD)){
                Button button = new Button();
                button.setText(r.getTitle());
                dinnerAccordion.getChildren().add(button);

            }else{
                Button button = new Button();
                button.setText(r.getTitle());
                supperAccordion.getChildren().add(button);


            }
        }

    }
}

package com.sda.organizer.controller;

import com.sda.organizer.domain.Step;

import com.sda.organizer.enums.Category;
import com.sda.organizer.service.RecipeService;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created by RENT on 2016-09-26.
 */
@Component
public class AddRecipeController extends ChangeScene {

    @FXML
    public ChoiceBox categorySelect;
    @FXML
    Button submitButton;

    @FXML
    TextField titleRecipe;

    @FXML
    TextArea descriptionRecipe;

    @FXML
    HBox newStep = new HBox();

    @FXML
    VBox stepsBox;

    @FXML
    Button removeStep;


    @FXML
    public void initialize() {
        addSteps();
        categorySelect.setItems(FXCollections.observableArrayList( Arrays.asList(Category.values())));
    }

    @Autowired
    private RecipeService recipeService;

    public void submitRecipe(ActionEvent actionEvent) {
        List<Step> steps;

        ///  Recipe recipe = new Recipe(titleRecipe.getText(), descriptionRecipe.getText());
        //  recipeService.addNew(recipe);
        newFxmlEvent(submitButton, "/cookbook.fxml");
    }

    public void addSteps() {
        HBox hBox = new HBox();
        TextField textField = new TextField();
        textField.setPromptText("Dodaj opis czynnosci");
        TextField textField1 = new TextField();
        textField1.setPromptText("Dodaj przewidywany czas");
        Button deleteSteps = new Button();
        deleteSteps.setText("-");
        deleteSteps.setOnAction(this::delSteps);

        hBox.getChildren().add(textField);
        hBox.getChildren().add(textField1);
        hBox.getChildren().add(deleteSteps);

        stepsBox.getChildren().add(hBox);
    }

    public void delSteps(ActionEvent event) {
        stepsBox.getChildren().remove(((Button) event.getSource()).getParent());
    }

}

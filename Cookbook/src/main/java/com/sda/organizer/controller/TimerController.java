package com.sda.organizer.controller;

import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.sda.organizer.service.RecipeService;

/**
 * Podstawowy kontroller dla widoku JavyFx.
 */
@Component
public class TimerController extends ChangeScene {

    @FXML
    public TableView recipeLIst;

    @Autowired
    RecipeService recipeService;

    private AnimationTimer timer;

    private static final Logger LOGGER = LoggerFactory.getLogger(TimerController.class);

    public void onValidate() {
        LOGGER.info("start");
    }

    @FXML
    Label timerLabel = new Label();
    @FXML
    TextArea recipeArea = new TextArea();
    @FXML
    private Button addRecipeButton;

    @FXML
    HBox newStep = new HBox();

    @FXML
    VBox stepsBox;

    @FXML
    public void initialize() {
        recipeLIst.setItems(FXCollections.observableArrayList(recipeService.getAll()));
    }

    public void startTimer() {
        timer = new AnimationTimer() {
            int count = 0;
            int countSec = 50;
            int countMin =0;
            int countHour =0;
            @Override
            public void handle(long now) {
                ++count ;

                if(count > 59) {
                    count = 0;
                    ++countSec;

                    if(countSec == 59){
                        ++countMin;
                        countSec = 0;
                    }
                    if(countMin == 60 ){
                        countHour ++;
                        countMin = 0;
                    }
                }

                timerLabel.setText(String.valueOf(convertToTimeFormat(countHour)
                        + ":" + convertToTimeFormat(countMin) +
                        ":" + convertToTimeFormat(countSec)));
            }
        };



        timerLabel.setOnMouseEntered(e -> {
            timerLabel.setScaleX(2);
            timerLabel.setScaleY(2);
        });

        timerLabel.setOnMouseExited(e -> {
            timerLabel.setScaleX(1);
            timerLabel.setScaleY(1);
        });

        timerLabel.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event ) {
                timer.stop();

            }
        });

        timer.start();
    }

    public String convertToTimeFormat(int value) {
        if(value>9) {
            return String.valueOf(value);
        }
        else {
            return String.valueOf("0"+value);
        }
    }

    public void addRecipe(){
        newFxmlEvent(addRecipeButton, "/addRecipe.fxml");

    }




}

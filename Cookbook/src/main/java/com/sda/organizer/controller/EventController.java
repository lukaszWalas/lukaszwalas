package com.sda.organizer.controller;

import com.sda.organizer.application.SpringFXLoader;
import com.sda.organizer.service.RecipeService;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by RENT on 2016-09-26.
 */
@Component
public class EventController {
    @FXML
    private Button addRecipeButton;

    @Autowired
    private RecipeService recipeService;

    public void addRecipe(){
        newFxmlEvent(addRecipeButton, "/addRecipe.fxml");

    }

    public void newFxmlEvent (final Node node, final String file){
        final Stage rootStage =(Stage) node.getScene().getWindow();
        final Scene newScene = new Scene(
                (Parent) SpringFXLoader.load(file), 1200, 600);
        rootStage.setScene(newScene);

    }
}

package com.sda.organizer.controller;

import com.sda.organizer.application.SpringFXLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by RENT on 2016-09-29.
 */
public abstract class ChangeScene {
    public void newFxmlEvent (final Node node, final String file){
        final Stage rootStage =(Stage) node.getScene().getWindow();
        final Scene newScene = new Scene(
                (Parent) SpringFXLoader.load(file), 1200, 600);
        rootStage.setScene(newScene);

    }
}


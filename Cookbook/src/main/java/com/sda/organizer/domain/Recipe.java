package com.sda.organizer.domain;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * Created by RENT on 2016-09-26.
 */
public class Recipe {
    @Id
    public String id;

    String title;
    String description;
    List<Step> steps;
    Enum category;


    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public Enum getCategory() {return category;}

    public Recipe(String title, String description, Enum category) {
        this.title = title;
        this.description = description;
        this.category = category;
    }
}

package com.sda.organizer;


import com.sda.organizer.application.SpringFXLoader;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application Launcher.
 */
public class Launch extends Application {

    private final static Logger LOGGER = LoggerFactory.getLogger(Launch.class);

    public static void main(final String[] args) {
        LOGGER.info("Start application");
        Application.launch();
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {

        BorderPane root = (BorderPane) SpringFXLoader.load("/cookbook.fxml");
        Scene scene = new Scene(root, 1200, 600);

        primaryStage.setTitle("CookBook");
        primaryStage.setScene(scene);
        primaryStage.show();


    }
}

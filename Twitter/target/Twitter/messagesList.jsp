<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>

</head>
<body>

	<%@ include file="/WEB-INF/jsp/common/navigation.jsp"%>

	<!-- Wlasciwa tresc strony -->
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Content</th>
				<th>Create date</th>
			</tr>
		</thead>
		<tbody>
		<!-- Iteracja po wiadomosciach. Za kazdym przejsciem
		petli dodajemy nowy wiersz do tabeli -->
		<c:forEach var="message" items="${requestScope.messagesList}">
			<tr>
				<td><c:out value="${message.id }" /></td>
				<td><c:out value="${message.content }" /></td>
				<td><c:out value="${message.date }" /></td>
			</tr>
		</c:forEach>
		</tbody>
	</table>

	<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>

</head>
<body>

	<%@ include file="/WEB-INF/jsp/common/navigation.jsp"%>

	<!-- Wlasciwa tresc strony -->
	<c:if test='${"SUCCESS" == requestScope.result}'>
		<span class="label label-success"> The message added
			succesfully</span>
	</c:if>

	<c:if test='${"ERROR" == requestScope.result}'>
		<span class="label label-error">Some exception occured</span>
	</c:if>

	<form action="addMessage" method="post" role="form">
		<div class="form-group">
			<label for="message">Message:</label> <input name="message"
				id="message" type="text" class="form-control">
		</div>

		<button type="submit" class="btn btn-default">Add message</button>
	</form>

	<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>
</body>
</html>
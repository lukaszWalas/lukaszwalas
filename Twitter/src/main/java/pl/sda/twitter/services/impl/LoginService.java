package pl.sda.twitter.services.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.sda.twitter.services.ILoginService;

public class LoginService implements ILoginService {

	@Override
	public boolean login(String login, String password) {
		// 1. Pobrac polaczenia do bazy danych
		Connection connection = null;
		try {
			// Zaladowanie sterownika bazy danych (w naszym przypadku
			// postgresql)
			Class.forName("org.postgresql.Driver");

			// Pobranie polaczenia do bazy danych
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
					"sylwia1983");
			// Wylaczenie opcji autoCommit - musimy commit wykonywac recznie
			connection.setAutoCommit(false);

			String GET_LOGIN_QUERY = "SELECT * FROM \"twitterUsers\" "
					+ "WHERE login = ? AND password = ?";

			try (PreparedStatement preparedStatement
					= connection.prepareStatement(GET_LOGIN_QUERY)) {

				preparedStatement.setString(1, login);
				preparedStatement.setString(2, password);

				ResultSet rs = preparedStatement.executeQuery();

				if (rs.next()) {
					return true;
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			// 4. Pozwalniac zasoby
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}
		return false;
	}

	@Override
	public void logout() {
	}

}
package pl.sda.twitter.services.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pl.sda.twitter.model.Message;
import pl.sda.twitter.services.IMessageService;

public class MessageService implements IMessageService {

	@Override
	public void insertMessage(String message) throws Exception {
		// 1. Pobrac polaczenia do bazy danych
		Connection connection = null;
		try {
			// Zaladowanie sterownika bazy danych (w naszym przypadku
			// postgresql)
			Class.forName("org.postgresql.Driver");

			// Pobranie polaczenia do bazy danych
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
					"sylwia1983");
			// Wylaczenie opcji autoCommit - musimy commit wykonywac recznie
			connection.setAutoCommit(false);

			// 2. Przygotowac zapytanie wstawiajace dane (INSERT)

			// Tresc zapytania do wykonania na bazie danych
			String INSERT_QUERY = "INSERT INTO messages (\"content\") VALUES (?)";

			try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY)) {

				preparedStatement.setString(1, message);

				// 3. Wykonac zapytanie na bazie danych
				preparedStatement.execute();
			}

			connection.commit();
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			System.out.println(e.getMessage());
			throw new Exception("Blad zapisu wiadomosci");
		} finally {
			// 4. Pozwalniac zasoby
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}
	}

	@Override
	public List<Message> getMessages() {
		List<Message> messages = new ArrayList<>();
		// 1. Pobrac polaczenia do bazy danych
		Connection connection = null;
		try {
			// Zaladowanie sterownika bazy danych (w naszym przypadku
			// postgresql)
			Class.forName("org.postgresql.Driver");

			// Pobranie polaczenia do bazy danych
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
					"sylwia1983");
			// Wylaczenie opcji autoCommit - musimy commit wykonywac recznie
			connection.setAutoCommit(false);

			String GET_MESSAGE_QUERY = "SELECT id, content, create_date FROM messages";

			try (PreparedStatement preparedStatement = connection.prepareStatement(GET_MESSAGE_QUERY)) {

				ResultSet rs = preparedStatement.executeQuery();

				while (rs.next()) {
					Message message = new Message();
					message.setId(rs.getInt(1));
					message.setContent(rs.getString(2));
					message.setDate(rs.getString(3));
					messages.add(message);
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			// 4. Pozwalniac zasoby
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}

		messages.sort(new Comparator<Message>() {

			@Override
			public int compare(Message o1, Message o2) {
				return o2.getDate().compareTo(o1.getDate());
			}

		});

		return messages;
	}

	@Override
	public void removeMessage(String id) {
		// 1. Pobrac polaczenia do bazy danych
		Connection connection = null;
		try {
			// Zaladowanie sterownika bazy danych (w naszym przypadku
			// postgresql)
			Class.forName("org.postgresql.Driver");

			// Pobranie polaczenia do bazy danych
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
					"sylwia1983");
			// Wylaczenie opcji autoCommit - musimy commit wykonywac recznie
			connection.setAutoCommit(false);

			// 2. Przygotowac zapytanie wstawiajace dane (INSERT)

			// Tresc zapytania do wykonania na bazie danych
			String DELETe_QUERY = "DELETE FROM messages WHERE id = ?";

			try (PreparedStatement preparedStatement = connection.prepareStatement(DELETe_QUERY)) {

				preparedStatement.setInt(1, Integer.valueOf(id));

				// 3. Wykonac zapytanie na bazie danych
				preparedStatement.execute();
			}

			connection.commit();
		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			System.out.println(e.getMessage());
		} finally {
			// 4. Pozwalniac zasoby
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}
	}

}
package pl.sda.twitter.services;

import java.util.List;

import pl.sda.twitter.model.Message;

/**
 * Interfejs dla uslug zwiazanych z wiadomosciami.
 */
public interface IMessageService {

	/**
	 * Zapisanie nowej wiadomosci.
	 * @param message Wiadomosc.
	 */
	void insertMessage(String message) throws Exception;

	/**
	 * Zwraca liste wiadomosci.
	 * @return Lista wiadomości.
	 */
	List<Message> getMessages();

	void removeMessage(String id);

}
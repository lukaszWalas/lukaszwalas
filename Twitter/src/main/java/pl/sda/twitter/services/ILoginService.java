package pl.sda.twitter.services;

public interface ILoginService {

	boolean login(String login, String password);

	void logout();

}
package pl.sda.twitter.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pl.sda.twitter.services.IMessageService;
import pl.sda.twitter.services.impl.MessageService;

public class AddMessageServlet extends HttpServlet {

	/**
	 * doGet obsuguje wszystkie ��dania GET skierowane do serwletu.
	 *
	 * @param req Reprezentuje ��danie uzytkownika przeslane przez
	 * przegl�darke (klient). Dostep do parametrow ��dania, nagl�wk�w HTTP.
	 *
	 * @param resp Reprezentuje odpowied� serwera. Dostep do naglowkow HTTP, tresci
	 * odpowiedzi.
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		/*
		 *  Miejsce na implementacje obslugi GET, przygotowanie obiektow
		 *  do zaprezentowania na stronie
		 */

		// Wlasciwe przekierowanie na strone ktora zostanie zaprezentowana klientowi.

		/*
		 * [req.getRequestDispatcher("addMessage.jsp")] => Pobranie strony do
		 * prezentacji
		 * [.forward(req, resp)] => Przekierowanie na pobrana strone
		 * (z zachowaniem stanu konwersacji)
		 *
		 * FORWARD => Przekierowanie wewn�trz serwera
		 * REDIRECT => Przekierowanie (wygenerowanie nowego ��dania)
		 */
		req.getRequestDispatcher("addMessage.jsp").forward(req, resp);
	}

	/**
	 * doPost obsuguje wszystkie ��dania POST skierowane do serwletu.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		/*
		 *  Miejsce na implementacje obslugi POST, przygotowanie obiektow
		 *  do zaprezentowania na stronie
		 */

		/*
		 * req.getParameter("message") => W ten sposob pobieramy parametry zadania (GET/POST)
		 * , przy czym req[obiekt reprezentujacy zadanie uzytkownika
		 * - klasa HttpServletRequest],
		 * "message"[nazwa parametru]
		 */
		String message = req.getParameter("message");
		System.out.println("Tresc wiadomosci: " + message);

		/*
		 * req.getSession() => W ten sposob pobieramy obiekt sesji HTTP
		 */
		HttpSession session = req.getSession();

		/*
		 * session.getAttribute("MessagesList") => Pobranie atrybutu z sesji
		 * - Zwracany jest obiekt typu Object [musimy rzutowac na wlasciwy typ]
		 * - session to jest obiekt implementuj�cy interfejs HttpSession
		 * - (List<String>) => rzutowanie na liste przechowujaca obiekty typu String
		 */
		List<String> messagesFromSession =
				(List<String>) session.getAttribute("MessagesList");

		// Zabezpieczenie przed NullPointerExcetpion
		if (messagesFromSession == null) {
			messagesFromSession = new ArrayList<>();
		}

		// Dodanie elementu do listy
		messagesFromSession.add(message);

		/*
		 * session.setAttribute("MessagesList", messagesFromSession) - dodanie
		 * atrybutu do sesji
		 * - do sesji dodawane sa atrybuty o typie Object
		 * - "MessagesList" - nazwa pod jakim dodajemy do sesji obiekt
		 */
		session.setAttribute("MessagesList", messagesFromSession);

		IMessageService messageService = new MessageService();
		try {
			messageService.insertMessage(message);
			req.setAttribute("result", "SUCCESS");
		} catch (Exception e) {
			req.setAttribute("result", "ERROR");
		}

		/*
		 * req.setAttribute("result", "SUCCESS") - dodanie atrybutu do obiektu request;
		 * - dzieki temu na stronie jsp bedziemy mieli dostep do tego atrybutu pod
		 * nazwa "result".
		 * - na jsp po pobraniu teg atrybutu dostaniemy wartosc "SUCCESS"
		 */
		// req.setAttribute("result", "SUCCESS");

		// Analogicznie jak w doGet(...)
		req.getRequestDispatcher("addMessage.jsp").forward(req, resp);
	}

}
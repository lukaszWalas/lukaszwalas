package pl.sda.twitter.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.sda.twitter.model.SessionData;
import pl.sda.twitter.services.ILoginService;
import pl.sda.twitter.services.impl.LoginService;

public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		if (req.getRequestURI().indexOf("/logout") != -1) {
			req.getSession().invalidate();
		}

		if (!"".equals(req.getParameter("logout"))) {
			req.getSession().invalidate();
		}

		req.getRequestDispatcher("login.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String login = req.getParameter("login");
		String password = req.getParameter("inputPassword");

		// Wywolanie serwisu ktory sprawdzi haslo i login uzytkownika w bazie
		ILoginService loginService = new LoginService();

		if (loginService.login(login, password)) {

			SessionData sessionData = new SessionData();

			// Miejsce na pobranie danych klienta

			sessionData.setLogin(login);
			req.getSession().setAttribute("SessionData", sessionData);

			// Przekierowanie jak OK
			resp.sendRedirect(req.getContextPath() + "/messagesList");
		}
		else {
			// W przypadku bledu
			req.setAttribute("Status", "ERROR");
			req.getRequestDispatcher("login.jsp").forward(req, resp);
		}

	}

}
package pl.sda.twitter.model;

/**
 * Reprezentuje wiadomosc w aplikacji.
 */
public class Message {

	/**
	 * Tresc wiadomosci.
	 */
	private String content;

	/**
	 * Identyfikator.
	 */
	private int id;

	/**
	 * Data dodania wiadomosci.
	 */
	private String date;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
function isPesel(pesel){
	var weights =[1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1],
	myPeselArr = [],
	sum = 0;
	
	if(pesel.toString().length != 11) return false;
	
	for (var i= 0; i<11; ++i){
		myPeselArr.push(parseInt(pesel.toString()[i]));
	}
	
	for (var i = 0; i<11 ; i++){
		sum += weights[i] * myPeselArr[i];
	}
	
	if(sum % 10 != 0)return false;
	
	sex = (myPeselArr[9] % 2 == 0) ? "K" : "M";
	day = myPeselArr[4]*10 + myPeselArr[5];
	month = (myPeselArr[2]%2)*10 + myPeselArr[3];
	year = 1900 + myPeselArr[0]*10 + myPeselArr[1];
	
	console.log(day, month, year, sex);
}

isPesel(91111314436);

function checkDivisionBy3(k){
	var x = k.toString().split("");
	var sum = 0;
	for (var i=0; i<=x.length; ++i){
		sum += x[i];
	}
	return (sum%3 === 0)? true: false;
}

function checkDivisionBy4(j){
	var x = j.toString().split("");
	var str = "";
	if (x.length>1){
		str += x[x.length-2];
	}
	str += x[x.length-1];	
		
	return (parseInt(str) % 4 === 0)? true: false;
}

function checkNumberFormat (num){
	return (typeof num == 'number' && (num|0 === num) && num > 0);
}

function printNumbers(num){
	if (checkNumberFormat(num)){
		for (var i = 0; i< num; ++i){
			if(!checkDivisionBy3(i) && !checkDivisionBy4(i)){
				console.log(i);
			} 
		}
	}
}